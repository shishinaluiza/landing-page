
$(document).ready(function () {
    // Navbar nach dem Click schließen  
    $(".navbar a").click(function(event) {
      $(".collapse").collapse('hide');
      console.log(2);
    });

    // fließendes Scrollen
    $(".navbar a").on('click', function(event) {

      if (this.hash !== "") {
        // Damit es nicht in einem Moment scrollt
        event.preventDefault();
  
        // Hash abspeichern
        let hrefContent = this.hash;
        
        // jQuery Funktion animate() fürs Scrollen benutzen.
        // Dabei bedeutet die Zahl 800 die Anzahl der Millisekunden, die für das Scrollen bis zur bestimmten Position notwendig sind.
        $('html').animate({
          scrollTop: $(hrefContent).offset().top
        }, 800, function(){
  
        });
      } 
    });
  });   
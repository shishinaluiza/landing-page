var gulp = require('gulp');
var concat = require('gulp-concat');
var minify = require('gulp-minify');
var cleanCss = require('gulp-clean-css');

function packJS() {
    return gulp.src(['vendor/jquery/jquery.min.js', 'vendor/bootstrap/js/bootstrap.bundle.min.js', 'script.js', 'vendor/cookieconsent/js/cookieconsent.min.js'])
        .pipe(concat('bundle.js'))
        .pipe(minify({
            ext:{
                min:'.js'
            },
            noSource: true
        }))
        .pipe(gulp.dest('build'));
}

function packCSS() {
    return gulp.src(['vendor/bootstrap/css/bootstrap.min.css', 'css/style.css', 'css/impressum.css', 'vendor/cookieconsent/css/cookieconsent.min.css'])
        .pipe(concat('stylesheet.css'))
        .pipe(cleanCss())
        .pipe(gulp.dest('build'));
}

function packPNG() {
    return gulp.src('assets/*.png')
        .pipe(gulp.dest('build/assets'))
}

function packSVG() {
    return gulp.src('assets/*.svg')
        .pipe(gulp.dest('build/assets'))
}

exports.default = gulp.series(packJS, packCSS, packPNG, packSVG);
